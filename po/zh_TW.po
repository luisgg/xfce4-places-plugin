# This file was automatically generated for the xfce4-places-plugin (2007)
# This file is distributed under the same license as the xfce4-places-plugin package
# It is based on the following:
# ###
# traditional Chinese translation for Thunar.
# Copyright (C) 2006 Benedikt Meurer.
# This file is distributed under the same license as the Thunar package.
# Hydonsingore Cia <hydonsingore@educities.edu.tw>, 2006.
# Cosmo Chene <cosmolax@gmail.com>, 2006.
#
# ###
# Chinese (Taiwan) translation of gnome-panel.
# Copyright (C) 1999-2007 Free Software Foundation, Inc.
# gnome-core:
# Yuan-Chung Cheng <platin@linux.org.tw>, 1999.
# Jing-Jong SHYUE <shyue@sonoma.com.tw>, 2000.
# Chih-Wei Huang <cwhuang@linux.org.tw>, 2000.
# Abel Cheung <abel@oaka.org>, 2001-2002.
# gnome-panel:
# Kevin Kee <kevin@oaka.org>, 2004
# Abel Cheung <abel@oaka.org>, 2002-2004.
# Woodman Tuen <wmtuen@gmail.com>, 2005-07
#
#
# ###
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-01-24 17:56+0100\n"
"PO-Revision-Date: \n"
"Last-Translator: Cheng-Chia Tseng <pswo10680@gmail.com>\n"
"Language-Team: \n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. vim: set ai et tabstop=4:
#: ../panel-plugin/places.desktop.in.h:1 ../panel-plugin/cfg.c:104
#: ../panel-plugin/cfg.c:217 ../panel-plugin/cfg.c:454
msgid "Places"
msgstr "位置"

#: ../panel-plugin/places.desktop.in.h:2
msgid "Access folders, documents, and removable media"
msgstr "存取資料夾、文件、可移除式媒體"

#. Trash
#: ../panel-plugin/model_system.c:155
msgid "Trash"
msgstr "垃圾桶"

#: ../panel-plugin/model_system.c:189
msgid "Desktop"
msgstr "桌面"

#. File System (/)
#: ../panel-plugin/model_system.c:205
msgid "File System"
msgstr "檔案系統"

#. TRANSLATORS: this will result in "<path> on <hostname>"
#: ../panel-plugin/model_user.c:242
#, c-format
msgid "%s on %s"
msgstr ""

#: ../panel-plugin/model_volumes.c:71
#, c-format
msgid "Failed to eject \"%s\""
msgstr "無法退出「%s」"

#: ../panel-plugin/model_volumes.c:120
#, c-format
msgid "Failed to unmount \"%s\""
msgstr "無法卸載「%s」"

#: ../panel-plugin/model_volumes.c:170 ../panel-plugin/model_volumes.c:193
#, c-format
msgid "Failed to mount \"%s\""
msgstr "無法掛載「%s」"

#: ../panel-plugin/model_volumes.c:478
msgid "Mount and Open"
msgstr "掛載並開啟"

#: ../panel-plugin/model_volumes.c:491
msgid "Mount"
msgstr "掛載"

#: ../panel-plugin/model_volumes.c:511
msgid "Eject"
msgstr "退出"

#: ../panel-plugin/model_volumes.c:521
msgid "Unmount"
msgstr "卸載"

#. TRANSLATORS: Please use the same translation here as in Thunar
#: ../panel-plugin/model_volumes_notify.c:128
msgid "Unmounting device"
msgstr "正在卸載裝置"

#. TRANSLATORS: Please use the same translation here as in Thunar
#: ../panel-plugin/model_volumes_notify.c:131
#, c-format
msgid ""
"The device \"%s\" is being unmounted by the system. Please do not remove the "
"media or disconnect the drive"
msgstr "裝置「%s」正被系統卸載。請不要移除該媒體，或是斷開該磁碟的連接"

#. TRANSLATORS: Please use the same translation here as in Thunar
#: ../panel-plugin/model_volumes_notify.c:138
#: ../panel-plugin/model_volumes_notify.c:262
msgid "Writing data to device"
msgstr "正在將資料寫入裝置"

#. TRANSLATORS: Please use the same translation here as in Thunar
#: ../panel-plugin/model_volumes_notify.c:141
#: ../panel-plugin/model_volumes_notify.c:265
#, c-format
msgid ""
"There is data that needs to be written to the device \"%s\" before it can be "
"removed. Please do not remove the media or disconnect the drive"
msgstr ""
"在裝置「%s」移除前，有資料需要寫入其中。請不要移除該媒體，或是斷開該磁碟的連"
"接"

#. TRANSLATORS: Please use the same translation here as in Thunar
#: ../panel-plugin/model_volumes_notify.c:253
msgid "Ejecting device"
msgstr "正在退出裝置"

#. TRANSLATORS: Please use the same translation here as in Thunar
#: ../panel-plugin/model_volumes_notify.c:256
#, c-format
msgid "The device \"%s\" is being ejected. This may take some time"
msgstr "裝置「%s」正在退出。這可能會花上一些時間"

#: ../panel-plugin/view.c:633
msgid "Network"
msgstr ""

#: ../panel-plugin/view.c:651
msgid "Connect to Server"
msgstr ""

#: ../panel-plugin/view.c:680
msgid "Search for Files"
msgstr "搜尋檔案"

#: ../panel-plugin/view.c:733 ../panel-plugin/cfg.c:603
msgid "Recent Documents"
msgstr "近期文件"

#: ../panel-plugin/cfg.c:490
msgid "Button"
msgstr "按鈕"

#: ../panel-plugin/cfg.c:499
msgid "_Show"
msgstr "顯示(_S)"

#: ../panel-plugin/cfg.c:505
msgid "Icon Only"
msgstr "僅圖示"

#: ../panel-plugin/cfg.c:506
msgid "Label Only"
msgstr "僅標籤"

#: ../panel-plugin/cfg.c:507
msgid "Icon and Label"
msgstr "圖示與標籤"

#: ../panel-plugin/cfg.c:520
msgid "_Label"
msgstr "標籤(_L)"

#: ../panel-plugin/cfg.c:536
msgid "Menu"
msgstr "選單"

#. MENU: Show Icons
#: ../panel-plugin/cfg.c:540
msgid "Show _icons in menu"
msgstr "於選單內顯示圖示(_I)"

#. MENU: Show Removable Media
#: ../panel-plugin/cfg.c:549
msgid "Show _removable media"
msgstr "顯示可移除式媒體(_R)"

#: ../panel-plugin/cfg.c:567
msgid "Mount and _Open on click"
msgstr "點擊時掛載並開啟(_O)"

#. MENU: Show GTK Bookmarks
#: ../panel-plugin/cfg.c:578
msgid "Show GTK _bookmarks"
msgstr "顯示 GTK 書籤(_B)"

#. MENU: Show Recent Documents
#: ../panel-plugin/cfg.c:588
msgid "Show recent _documents"
msgstr "顯示近期文件(_D)"

#. RECENT DOCUMENTS: Show clear option
#: ../panel-plugin/cfg.c:607
msgid "Show cl_ear option"
msgstr "顯示清除選項(_E)"

#: ../panel-plugin/cfg.c:619
msgid "_Number to display"
msgstr "要顯示的數目(_N)"

#: ../panel-plugin/cfg.c:639
msgid "Search"
msgstr "搜尋"

#: ../panel-plugin/cfg.c:647
msgid "Co_mmand"
msgstr "指令(_M)"

#: ../panel-plugin/support.c:155
msgid "Open"
msgstr "開啟"

#: ../panel-plugin/support.c:170
msgid "Open Terminal Here"
msgstr "在這裡開啟終端機"

#: ../panel-plugin/xfce4-popup-places.sh:28
msgid "Usage:"
msgstr "用法："

#: ../panel-plugin/xfce4-popup-places.sh:29
msgid "OPTION"
msgstr "選項"

#: ../panel-plugin/xfce4-popup-places.sh:31
msgid "Options:"
msgstr "選項："

#: ../panel-plugin/xfce4-popup-places.sh:32
msgid "Popup menu at current mouse position"
msgstr "在滑鼠的目前位置彈出選單"

#: ../panel-plugin/xfce4-popup-places.sh:33
msgid "Show help options"
msgstr "顯示幫助選項"

#: ../panel-plugin/xfce4-popup-places.sh:34
msgid "Print version information and exit"
msgstr "列印版本資訊後離開"
